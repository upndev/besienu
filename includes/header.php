<!DOCTYPE html>
<html lang="lt" itemscope itemtype="http://schema.org/Other">
<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Example</title>
	<meta name="robots" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<?php /*
	<link rel="icon" href="img/favicon.ico">
	<link rel="canonical" href="http://www.example.com/">
	<meta name="keywords" content="keywords">
	<meta name="description" content="description">
	<meta name="author" content="author">
	<!-- place -->
	<meta name="geo.region" content="LT-VL">
	<meta name="ICBM" content="54.687156, 25.279651">
	<meta name="geo.placename" content="Vilnius">
	<meta name="geo.position" content="54.687156;25.279651">
	<meta name="dc.language" content="lt">
	<!-- itemprop -->
	<meta itemprop="name" content="Example">
	<meta itemprop="description" content="description">
	<meta itemprop="image" content="http://www.example.com/images/example.jpg">
	<!-- image -->
	<meta property="og:image" content="http://www.example.com/images/example.jpg">
	<meta property="og:image:secure_url" content="https://www.example.com/images/example.jpg">
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="630">
	<!-- og main -->
	<meta property="fb:app_id" content="33333333">
	<meta property="og:title" content="Example">
	<meta property="og:description" content="description">
	<meta property="og:type" content="profile">
	<meta property="og:site_name" content="Example Parent">
	<meta property="og:locale" content="lt_LT">
	<meta property="og:locale:alternate" content="en_US">
	<meta property="og:locale:alternate" content="ru_RU">
	<meta property="og:url" content="http://www.example.com/">
	<!-- video -->
	<meta property="og:video" content="http://example.com/movie.swf" />
	<meta property="og:video:secure_url" content="https://secure.example.com/movie.swf" />
	<meta property="og:video:type" content="application/x-shockwave-flash" />
	<meta property="og:video:width" content="400" />
	<meta property="og:video:height" content="300" />
	<!-- audio -->
	<meta property="og:audio" content="http://example.com/sound.mp3" />
	<meta property="og:audio:secure_url" content="https://secure.example.com/sound.mp3" />
	<meta property="og:audio:type" content="audio/mpeg" />
	<!-- twitter -->
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@mytwitter" />
	<meta name="twitter:title" content="Example" />
	<meta name="twitter:description" content="description" />
	<meta name="twitter:image" content="https://www.example.com/images/example.jpg" />
	<meta name="twitter:url" content="https://www.example.com/" />
	<!-- apple -->
	<link rel="apple-touch-icon" href="touch-icon-iphone.png"> <!-- 60 x 60 -->
	<link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
	<link rel="apple-touch-startup-image" href="startup.png" /> <!-- 320 x 480 -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<!-- android -->
	<link rel="shortcut icon" sizes="196x196" href="touch-icon-android-retina.png">
	<link rel="shortcut icon" sizes="128x128" href="touch-icon-android.png">
	*/ 
	$dev = strpos($_SERVER['HTTP_HOST'], 'upndev.com') === false;
	if ($dev) {
		require 'vendor/less-compiler/Less.php';
		require 'vendor/replacer.php';
		$content = file_get_contents('less/frontend.less');
		$parser = new Less_Parser([
			'compress'=>true,
			'sourceMap'=>true,
			'sourceMapWriteTo'=>'css/frontend.min.map',
			'sourceMapURL'=>'css/frontend.min.map',
		]);
		//$content = Replacer::get($content);
    	$parser->parse($content);
		$parser->ModifyVars([
			'dir' => '"'.__DIR__.'/../less/"',
			'url' => '"../"'
		]);
		$css = $parser->getCss();
		file_put_contents('css/frontend.min.css',$css);
	}
	?>
	<link rel="stylesheet" href="css/font-awesome-animation.min.css">
	<link rel="stylesheet" href="css/perfect-scrollbar.css">
	<link rel="stylesheet" href="css/photoswipe.css">
	<link rel="stylesheet" href="css/default-skin/default-skin.css">
	<link rel="stylesheet" href="css/frontend.min.css">
</head>
<body>
<div class="header">
    <div class="wrap">
		<a class="logo" href="#">Mieste. Gatvėj. Galerija be sienų</a>
		<a class="back-button" href="#"></a>
		<a class="menu-burger">
			<svg version="1.1" id="Layer_6" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				viewBox="0 0 26.2 24.5" style="enable-background:new 0 0 26.2 24.5;" xml:space="preserve">
			<style type="text/css">
				.st0{fill:#4D4C4D;}
			</style>
			<g>
				<path class="st0" d="M0,21.8c0,1.4,1.2,2.6,2.6,2.6h21c1.4,0,2.6-1.2,2.6-2.6s-1.2-2.6-2.6-2.6h-21C1.2,19.2,0,20.4,0,21.8z"/>
				<path class="st0" d="M0,12.2c0,1.4,1.2,2.6,2.6,2.6h21c1.4,0,2.6-1.2,2.6-2.6S25,9.6,23.6,9.6h-21C1.2,9.6,0,10.8,0,12.2z"/>
				<path class="st0" d="M2.6,0C1.2,0,0,1.2,0,2.6s1.2,2.6,2.6,2.6h21c1.4,0,2.6-1.2,2.6-2.6S25,0,23.6,0H2.6z"/>
			</g>
			</svg>
		</a>
		<div class="main-menu">
	        <ul>
	            <li>
	                <a href="#"><span>Apie</span></a>
	            </li>
	            <li>
	                <a href="#"><span>Naujienos</span></a>
	            </li>
	            <li>
	                <a href="#"><span>Renginiai</span></a>
	            </li>
	            <li>
					<a href="javascript:void(0)" class="menu-dropdown"><span>Prisidėk</span></a>
					<ul class="dropdown">
						<li><a href="#">Miesto akupunktūra</a></li>
						<li><a href="#">Miestiečių manifestas</a></li>
					</ul>
	            </li>
	            <li>
	                <a href="#"><span>Galerija</span></a>
	            </li>
	            <li>
	                <a href="#"><span>Kontaktai</span></a>
	            </li>
	        </ul>
	    </div>
    </div>
</div>