var setLayouts = debounce(function() {
	// do stuff
}, 250);
var $d=$(document);

$(function(){
	FastClick.attach(document.body);
	if ($(window).width() > 768 && $('.posts-container').length) {
		var ps = new PerfectScrollbar('.posts-container');
	}
	
	initPhotoSwipeFromDOM('.gallery-images');

	if ($('.am').length) {
		$('.am').eq(0).addClass('faa-float animated');
		setTimeout(function() {
			$('.am').eq(1).addClass('faa-float animated');
		}, 300);
		setTimeout(function() {
			$('.am').eq(2).addClass('faa-float animated');
		}, 500);
		setTimeout(function() {
			$('.am').eq(3).addClass('faa-float animated');
		}, 700);
		setTimeout(function() {
			$('.am').eq(4).addClass('faa-float animated');
		}, 1);
		setTimeout(function() {
			$('.am').eq(5).addClass('faa-float animated');
		}, 500);
		setTimeout(function() {
			$('.am').eq(6).addClass('faa-float animated');
		}, 500);
		setTimeout(function() {
			$('.am').eq(7).addClass('faa-float animated');
		}, 500);
		setTimeout(function() {
			$('.am').eq(8).addClass('faa-float animated');
		}, 700);
		setTimeout(function() {
			$('.am').eq(9).addClass('faa-float animated');
		}, 1);

	} 

});

$(window).resize(setLayouts);

// debounce
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	}
}

// ref https://github.com/WICG/EventListenerOptions/pull/30
function isPassive() {
    var supportsPassiveOption = false;
    try {
        addEventListener("test", null, Object.defineProperty({}, 'passive', {
            get: function () {
                supportsPassiveOption = true;
            }
        }));
    } catch(e) {}
    return supportsPassiveOption;
}

$d
.on('click', '.js-about', function() {
	if($(this).hasClass('active')) {
		$(this).removeClass('active');
		$('.js-about-content').stop().slideUp();
	}
	else {
		$(this).addClass('active');
		$('.js-about-content').stop().slideDown();
		$('.js-participate').removeClass('active');
		$('.js-participate-content').stop().slideUp();
	}
})
.on('click', '.js-participate', function() {
	if($(this).hasClass('active')) {
		$(this).removeClass('active');
		$('.js-participate-content').stop().slideUp();
	}
	else {
		$(this).addClass('active');
		$('.js-participate-content').stop().slideDown();
		$('.js-about-content').stop().slideUp();
		$('.js-about').removeClass('active');
	}
})
.on('click', '.js-elements', function() {
	$('.element-list').slideToggle(200);
})
.on('click', '.main-menu > ul > li > a', function() {
	$(this).parent().toggleClass('active');
})
.on('click', '.menu-burger', function() {
	$('body').toggleClass('menu-opened');
});

function imageMovement(){
	$('leaf-image').delay(500).addClass()
};
