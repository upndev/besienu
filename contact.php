<?php include 'includes/header.php'; ?>

<div class="wrapper">

    <div class="wrap">

        <div class="contact-img">
            <img src="images/nuotrauka.jpg" alt="Contact image">
        </div>

        <div class="contact-text">
            <p>
                Vasaros paviljonas
                <br>
                Kauno paveikslų galerija
                <br>
                K.Donelaičio g. 16, Kaunas
            </p>
            
            <p>
                Telefonas: 8 37 22 17 79
                <br>
                besienu@gmail.com
            </p>
        </div>

    </div>
    <div class="wrap">
        <div class="contact-back-button">
            <div class="go-back">
                <a class="back-button-events" href="#"></a>
            </div>
        </div>
    </div>
    
    <div class="contact-footer">
        <footer class="event-footer">
            <div class="wrap">
                <div class="copyright">
                    <span>Projektą remia:</span>
                    <div class="footer-img1">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="94" height="45" viewBox="0 0 94 45">
                            <image xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF4AAAAtCAMAAAAQuiwzAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAC0FBMVEX///9/f39sbGxtbW2SkpJgYGDw8PDc3NxISEhjY2PHx8e4uLjJycn6+vri4uLW1ta7u7v39/fk5OTZ2dnn5+fh4eHo6Ojl5eW3t7fd3d35+fmysrLp6eny8vL+/v7FxcWrq6vu7u7r6+uzs7O0tLTm5ubx8fH7+/vj4+Pq6urGxsbCwsLY2NjNzc2vr6/AwMBGRkZra2slJSUwMDD9/f08PDyNjY2VlZUyMjIgICDV1dUjIyNnZ2f4+PhbW1ukpKRdXV3t7e0YGBiIiIgiIiIXFxckJCRvb28eHh5LS0vv7+8bGxt7e3stLS3X19fPz8+FhYXExMQzMzOfn5/29vZFRUVPT09wcHBUVFQWFhba2tomJiaDg4Nzc3O9vb1ERETR0dExMTFNTU0hISHIyMgvLy+Kioo/Pz+5ubmGhoYfHx/Dw8McHBwqKio0NDRMTEyurq6UlJSMjIxVVVXKyspTU1NHR0dhYWHg4OAdHR3Ly8unp6dWVlaJiYlfX193d3eCgoK+vr78/PycnJwUFBRRUVFaWlo7Ozs+Pj54eHgpKSleXl46OjouLi6bm5s2Nja6uroTExN9fX21tbUoKCgSEhKampo5OTmOjo41NTWhoaGioqKgoKBYWFjf398rKyt1dXUnJyesrKyYmJi/v7+qqqrs7OzQ0NBmZmaxsbFKSkpCQkLT09NoaGhlZWWPj489PT3BwcFQUFDS0tJXV1elpaX09PS2trY4ODh5eXmdnZ2AgIDb29tpaWnz8/Ojo6OLi4tycnKQkJCHh4eenp68vLz19fVOTk6oqKiZmZnOzs5BQUFiYmLe3t6BgYFJSUlZWVlDQ0Otra1AQEAODg4REREQEBCmpqaWlpYVFRVSUlKRkZEaGhp+fn58fHxcXFx0dHQsLCyEhITMzMw3Nzepqalqamp2dnZ6enpubm6wsLBkZGSTk5NxcXEZGRkAAADUxqh9AAAAAWJLR0TvuLDioQAAAAlwSFlzAAAPYQAAD2EBqD+naQAAB9tJREFUWMPt1olTE1keB/CfyB3RIKJyC6jcok0MxAaFEBsIwnIuSmAAwxkIXgMBOYVVkUPcAQW5jwEFj2FAQBCiBgSHQdRld9tFdBDBa4+/YV93EsAaa9edLWtrq+ZbBd106M/r/Pq9XzfAr/mfZZXKl8tqVVBT/3LR0AQt0Gas0WEw1q5jMHUZjPXrGQy9DQwGQ38jQ55NAJsNDI2MwcRIH8DUbIu5sZEFGFpuNQRDo22weTtYGVkDGNkAMPTBVhvs7MHMAXR3OJpu3In4XZgTazfGdsZc9DmcPasw3M4Vw3a77cXk2Qfg5O7BZcJmzBPAnbfflklogxfH28eU73sA/PzhN7wAAE4gQFAwuBMhoZ7w2zA4iB/SoflwAb41Aov8Co+KxmPQQIfthZggNoC3zMfxohEfn6AGibwkQzfE80WJdqJkforYBPHs1CPLvOAozbtxj5mz5Pxx4sTXeJp5SrojJsnIFJ48gAmzjDjYMu+XnYP43LwgyPfVAUD8KREUcAv5v0tzRvzpM766S/xprXT66s8WeQDNnzNMzbSJOwvFaUwsKZjwcYvDSkJKsRV82fk0xJdf+D3/GxxovqISIMWZf/GSZpW/lbBa5LDEe14+TfPR4gQ3mtesqSXqAnhu6fWWRKIWkVVNXG44ga3k49b4NDI3+TY1t7RyvqV5e66NcUob/2J7XJn/FeFV79plvqmc5qGD+JbmOwUbrp2x8b6udqMDa/dutBAI1sUv6xyaZ2PM2svs0KaNzYKb16lbW6SZjRvzL5p8x/OvEVd3nQNhdr6c/x6n+e5cTg/NM7FzPYL2W73r/NXVTfvUbbL67RU0fnugzd8KwEvP4Eb0YDVk3QH7PPeh6GF7cHC9agvS3BD+Vet9EXD3HrgOR8D9UyBrKVh9FEYKITJ3VF57gx2c8CbZ+Tt1noMR+1axhlB1LapjkH7WqOC/6wg0f6tHFtpQujbtgaz65A3b0Z3S0XaAseDxYDf0H8muD8Gs4xemthPx2eKM+7WFZjczfmCDSojYfhgjjimGn/gRR7e2RfbzTH6XK/u3STqMeJv76eYDUm0Nqw2PxiqmLH9A6wl/7IBw/gGBfOZ8IrFPZJ9ZnNU2W56q99WluRSn5/DNJuR3VVAB6k+VE/NTqbD5+bG+kOV9lrqC7yy70jBRTtDWrnYX5YT8w3S5SMFPch18Luqic6R/NJgKos8e5p4Zv+k6DlOhOft5x9lF0PYnj2CRt8zkzxFwKLySXFfG1SqQ85j42dehNF+659ryjCdZQ6flfLeLJVGZQV3eX2IFzpQeIA42m1EJOwFJPmzNtsznUdAye1L44qUokNsFHA0XsmS8hpel4DHsJ905HMNerV0tl4Xzh4lBpIyF0nxXfYRTkRnFH3LH+RT/qglAyY+/9pVRfOvsdivv49xUOx7ii3fAzNwST5QGdizg/c6KNukFIa0sivHQpHtOvE/a00WKn/W7TR0f43ou85VYvCrFs7kAt19yJycbNVx6M2NB7c0SjwZwtmm5oGiTZduX7tBDYh+4HVojztd8RhfnDk8bbU3FKhQfeRCSjrCLhCTNX5pVd8vM4baIUtHVN7ZA99wKHsPvsSYxjNdcTAjW0LK2k1dIHVWcjP19grszeV4NiF8XM5CfBZCDB1TMqMQvJI67s8eHb6mWt+bPOuBvI0VmXEYkVfvc9FbO4kq+Uh8eYrzXALveyS8cHZ/mUHygGHbqSfYKJqBP2PB89zT1+Utv9/ow82vcRkdZWjQvr7GxnufRizVfMhEyG3gaC4tDVSnvTVfwlckAi9hbdObVbbQuTcE+nvemH21Z9KbAFFaEtfKvAuW8p5Xn6EhvM7VWHiF6CsBAqOQrzv/CyI4o+ShDBNcpF/pAEcAVXMm3qX6ceRXVzwvdcyi+ahOlJisa8GsMLU09apKe/VRT6L//nzRkxFOPuBXRwlJ0gT+L1pnU4jTiWzrQ6MakVIc0OMWADJLs8RhEPc+KfAAfLIHcsJE8SZJkNLm+gyT7rnwYIguuM4B5t0/B8559PKr0PScWIAET9gJsf4jecxaIEWgltki55EwnkJUJqi6lJwC2eU/3zabCbKAt8U1REOeUcGuXX+m2+YU6ri77cbCgs1/BD1Mm3QHHFANk5QFYprxAey3Us7aiUNTw5PKcgu+CHpyJFm8057ZjqYhF8Y7wpsQA8egRruDdJaCYmF2UfL4O/Wp3VX4BqrN4oB+TH2n+EecDIalS8K8m+526jQESudk5N/e3zwZqE44mgkCKL/2rkh/EauW170avjtAj+ICKmT6Ddifu0gNY0GWie05chTVnJKUwpUHOx9yLDo/ZXQAt+Lvu55V7aP5v00DxmveUPIQJ5cW5ih7ea12IF/39f8cOIvIob9IaWFP16Os7dMnfcyqCOS+FocRRxPsAWQRuOrWZG8C56lIl0yXRd7Md8VVqNc17ofW8UPMPacBjE53KVfLiiCJNnih6vHjv3r2ILEt8RxBRg/mKh6FTOacpb9whKADxApcbgqAXcTGNOpCfbjnr0VUXVXpN2EbEBY0ivthPxak7mdMpfvMuAa+l+Y7Bf5lw1IXCamGkBpKOuX8/IpE4SyS9u7R6AEbnw0vgbf+ElsRxUSKRXMh9PScpCZjUg+Az760f/DRvhXg14y+X65owefjLZXzgM5f3r/n/yz8BVVKxvlSCI08AAAAASUVORK5CYII=" width="94" height="45"/>
                        </svg>
                    </div>
                    <div class="footer-img2">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="87" height="34" viewBox="0 0 87 34">
                            <image xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAAAiCAMAAAAd+bUsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACTFBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoMCkSAAAAw3RSTlMA8MDkQMSwkBBU9NDPw6UOkXkUnIIlqJOfQ6JMnjpFm0hglQYVnR+PiSmBfHQwcKAxPlKAXgikAaMMqhILphaME3aLImFbSTVxfWUDiJYKdRcFabNjHYpOOW0CbpohNC17U8gnGxEEKDi+siQNho6EcnpEdxlojTdGIxggKhwJLwe8oTa6rlCHb1dafng7XDNnmU2xUedixywmf6f7vUpsc/XbT2Yaq6y3lIVBPD3M5ljT8x4/xl+Ykpe4uanJtK2Dtg/MryWkAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAAPYQAAD2EBqD+naQAAA/FJREFUSMe11otb01YYB+CfJTQFlLLVFuQyCaVCW1LQttSOUbFeANtROsBJmVJXKGC5TGBDiaIbWCaWOXAypxvu5i5Md3NednXbX7av7TbT2vBowe9pkpOT5O3Jl3Nygg0yWQakgsmQZcqRVshiPymXhSJ9N+tpuNk5ORtlUrGRZTflxs9T5j0DPKvarNbkswVbNKyGLSwqLsFzW0u5Mi3KdUXbKirz9Cg3GKtETZYInn1Y5nhamXhUK1CzfQfMsKCaalhYawHbTvvzdXih3rGrAbvTdqNhpiXuwrkHzN59+2mnsan5gAtpum4zk+iq3XixxaOnnVbW+1Jb+7q0N58WTcdBZL1MhUOddiDXl+BWyOXyrljpFSq1p3KZw3G35Ejc1XXjkJ+2R18NQFvbg976BjeCuYntPcJkM32xUv/AMSaU7A46h8zDNcOdgZHXTMdDGGUbqW6k0uOlY94hWvWO+Y3jeP2N/InkPGz4d+s+pk2RhyePJPchu66uiH0s94TJdPIxXDErdl0KJSYFpSD4IAgKwQE7T3GqRTiNKZ4PCkJ57CmcUdJ6/MzZZNf4pogVu731B+B/S9CZXGjSqdumwRRzM9y51rYw7LNDUyrjXl0P3q48P+vB3IWsgiQ3MtCC1C5MwoTTFu3A4E3R0aBw0PowWOrO4TA4BvPvXPTTeZ7pYhe2JLqRge6dNin33TwXRC5iY+1/t2jPQt9YtJ/2NS9eei8odi/vHmgJZpqk3KVCupfOGqrtErnD5O5YBOd8/woiAar44Crs3DWx++FHdGHH5YtSeQjptCjdB8wtR8XBuHt9HjAEaDQKKlv/x1TRNT8KFIrdT2K5Xfx0MJVrdYbx2bXxoObzJY3rRls4/AX0XzroeX7V7vkaXrcFpStT/gulRgMWvrGUPdp/wdancu1MP0LMafgmJnyooy7WjSvMJB3QzlPKrcxNoMaO8Vs36K9GS2wpXO23S6ny8OSR/H44+Z3+qbj4/gfr+rkB/kf+drzi1IyO70lyp1kK6gyWVmD7cVPX5P6VXizPDo4Nba6Kjt0mn4Srovk3Eq/QUPF6kpulFjhchVVO0zJNFA25qK5D1U9gOOjzgNCdZck8PBoJeSAXuHvrni82AW2FcH8bTULk2qlXuW//vCZXhaPUsF9WFs7Tm+DXBnLlhjslOKtCc8ca3O5Mtoy6N+uopfkneB+x9moLMXeJrTaswa2gEVbZQwO48SDi45hcmwoFNBB+C4ovzZBx0t9Rm36Xyf7478xIsZKfoTfWSOXog5u4y8HyQA3sumf+c/rEX3/De24pqbE5ku2dLF3tblaNVd1oHtJ06bs6W9rNoGSkxf4Dr7/UNksCB9MAAAAASUVORK5CYII=" width="87" height="34"/>
                        </svg>
                    </div>
                    <span class="copy">Copyright &copy; 2018 Savininko pavadinimas. All rights reserved</span>
                </div>
            </div>
        </footer>
    </div>
</div>


<?php include 'includes/footer.php'; ?>