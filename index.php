<?php include 'includes/header.php'; ?>
<div class="wrapper">
    <div class="ground">
        <div class="manifest">
            <h2>MANIFESTAS</h2>
            <p>Įvardinti miesto prioritetus ir vertybes – svarbus žingsnis įvirtinant miestiečių teises. Kauniečių ir Kauno miestui prijaučiančiųjų balsas – tai miesto siela palaikanti jo gyvybę. Kviečiame prisidėti prie miestiečių manifesto gimimo: įvardinkite, kas jums svarbu, kad jaustumėtės gyvenąs pilnavertišką ir kūrybingą gyvenimą Kauno mieste šiandien. Užbaikite sakinį!</p>
        </div>
    </div>
    <div class="tree">
        <div class="leaf-image am" style="background-image: url('img/leaf.svg');"></div>
        <div class="spider-image am" style="background-image: url('img/spider.svg');"></div>
        <div class="open-image am" style="background-image: url('img/open.svg');"></div>
        <div class="small-leaf1 am" style="background-image: url('img/small-leaf1.svg');"></div>
        <div class="small-leaf2 am" style="background-image: url('img/small-leaf2.svg');"></div>
        <div class="eyes-image am" style="background-image: url('img/eyes.svg');"></div>
        <div class="mouth-image am" style="background-image: url('img/mouth.svg');"></div>
        <div class="chin-image am" style="background-image: url('img/chin.svg');"></div>
        <div class="wrap">
            <div class="text1 text">
                Morbi non sapien aliquet, luctus libero sed, ornare lectus. Nam mi velit, porttitor a augue non, consequat vestibulum lectus. Cras sit amet sem ut est faucibus posuere vel a erat. Duis eu mi ut nisi semper hendrerit. Cras vel nulla sed ligula suscipit condimentum. Proin maximus sagittis erat, vel maximus diam venenatis eget. Vivamus sollicitudin rhoncus ex, vel
            </div>
        </div>

        <div class="text2 text">
            KVIEČIAME APSILANKYTI PROJEKTO PAVILJONE!
            <br>
            <span>K. Donelaičio g. 16, Kaunas</span>
        </div>

        <div class="text3 text">
            INTERAKTYVUS ŽEMĖLAPIS “MIESTO AKUPUNKTŪRA”
        </div>

        <div class="col-1 col">
            <h2>Naujienos</h2>

            <div class="post-container">
                <a class="post-link">Straipsnio antraštė apie projekto reikalus</a>
                <a class="more-button" href='#'></a>
            </div>

            <div class="post-container">
                <a class="post-link">Straipsnio antraštė apie projekto reikalus</a>
                <a class="more-button" href='#'></a>
            </div>

            <div class="post-container">
                <a class="post-link">Straipsnio antraštė apie projekto reikalus</a>
                <a class="more-button" href='#'></a>
            </div>
        </div>

        <div class="col-2 col">
            <h2>Renginiai</h2>

            <div class="post-container">
                <span>01.06.2018</span>
                <br>
                <a class="post-link post-link-event">Renginio pavadinimas Renginio pavadinimas Renginio pavadinimas Renginio pavadinimas</a>
                <a class="more-button" href='#'></a>
            </div>

            <div class="post-container">
                <span>01.06.2018</span>
                <br>
                <a class="post-link post-link-event">Renginio pavadinimas</a>
                <a class="more-button" href='#'></a>
            </div>

            <div class="post-container">
                <span>01.06.2018</span>
                <br>
                <a class="post-lin post-link-event">Renginio pavadinimas</a>
                <a class="more-button" href='#'></a>
            </div>
        </div>
        
        <div class="hand am" style="background-image: url(img/hand.svg);"></div>
        <div class="branch am" style="background-image: url(img/branch.svg);">

        </div>
        
    </div>

    <div class="share">
        <div class="wrap">
            <div class="share-container">
                <div class="message">
                    <span> Man ir mano miestui rūpi, kad... </span>
                    <br>
                    <textarea placeholder="Pabaigti sakinį"></textarea>
                </div>

                <div class="name">
                    <span>VARDAS (pasirinktinai)</span>
                    <br>
                    <textarea placeholder="Įrašyti"></textarea>
                </div>

                <a class="share-button" href="#">Dalintis</a>
            </div>
        </div>

        <div class="flag">
            <svg version="1.1" id="f" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 181.8 339.5" style="enable-background:new 0 0 181.8 339.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#4D4C4D;stroke-width:5.6693;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
                .st1{fill:none;stroke:#4D4C4D;stroke-width:6.8031;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
            </style>
            <g>
                <g>
                    <path class="st0" d="M6.9,336.6c-2.3,0-4.1-1.8-4.1-4.1v-268c0-2.3,1.8-4.1,4.1-4.1s4.1,1.8,4.1,4.1v268
                        C11,334.8,9.2,336.6,6.9,336.6z"/>
                </g>
                <g>
                    <path class="st1" d="M25.4,73.1c0,0,9.1,1.1,12.7-9.7c3.6-10.9-7.5-56.8,52.3-59.8c24.3-1.2,47,5.2,58.6,33.8
                        c7.1,17.6,17.8,33,29.4,35.8c-11.6,2.8-22.3,18.2-29.4,35.8c-11.6,28.5-34.3,35-58.6,33.8c-59.8-3.1-48.7-49-52.3-59.8
                        C34.5,72,25.4,73.1,25.4,73.1"/>
                    <line class="st1" x1="172.5" y1="73.1" x2="12.8" y2="73.1"/>
                    <g>
                        <line class="st1" x1="150.2" y1="41.8" x2="101.9" y2="73.1"/>
                        <line class="st1" x1="150.2" y1="104.3" x2="101.9" y2="73.1"/>
                    </g>
                    <g>
                        <line class="st1" x1="124.3" y1="10.7" x2="71.3" y2="73.1"/>
                        <line class="st1" x1="124.3" y1="135.5" x2="71.3" y2="73.1"/>
                    </g>
                    <g>
                        <line class="st1" x1="92.2" y1="4.3" x2="42.8" y2="73.1"/>
                        <line class="st1" x1="92.2" y1="141.9" x2="42.8" y2="73.1"/>
                    </g>
                </g>
            </g>
            </svg>
        </div>

    </div>

<div class="comments">

    <div class="wrap">
        <div class="comment-container" data-columns>
            <div class="item">
                <p>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam bibendum eros vitae diam suscipit, eu egestas metus ultrices. In elit sapien, accumsan sed accumsan vel, mollis ac erat. Nulla at diam a nunc bibendum imperdiet. Suspendisse nec turpis hendrerit mi lacinia consectetur vitae eu sem. In sed velit quam. Donec interdum est in molestie iaculis. Vestibulum urna leo, scelerisque ut velit id, pellentesque fermentum lacus. Ut eu enim quis tellus luctus aliquam. 
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>
            
            <div class="item">
                <p>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam bibendum eros vitae diam suscipit, eu egestas metus ultrices.
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>

            <div class="item">
                <p>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam bibendum eros vitae diam suscipit, eu egestas metus ultrices. In elit sapien, accumsan sed accumsan vel, mollis ac erat. Nulla at diam a nunc bibendum imperdiet. Suspendisse nec turpis hendrerit mi lacinia consectetur vitae eu sem. In sed velit quam. Donec interdum est in molestie iaculis. Vestibulum urna leo, scelerisque ut velit id, pellentesque fermentum lacus. Ut eu enim quis tellus luctus aliquam. 
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>

            <div class="item">
                <p>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam bibendum eros vitae diam suscipit, eu egestas metus ultrices. In elit sapien, accumsan sed accumsan vel.
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>

            <div class="item">
                <p>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam bibendum eros vitae diam suscipit, eu egestas metus ultrices. In elit sapien, accumsan sed accumsan vel, mollis ac erat. Nulla at diam a nunc bibendum imperdiet. Suspendisse nec turpis hendrerit mi lacinia consectetur vitae eu sem. In sed velit quam. Donec interdum est in molestie iaculis. Vestibulum urna leo, scelerisque ut velit id, pellentesque fermentum lacus. Ut eu enim quis tellus luctus aliquam. 
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>

            <div class="item">
                <p>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam bibendum eros vitae diam suscipit, eu egestas metus ultrices.
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>

            <div class="item">
                <p>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam bibendum eros vitae diam suscipit, eu egestas metus ultrices.
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>

            <div class="item">
                <p>
                    Suspendisse nec turpis hendrerit mi lacinia consectetur vitae eu sem. In sed velit quam. Donec interdum est in molestie iaculis. Vestibulum urna leo, scelerisque ut velit id, pellentesque fermentum lacus. Ut eu enim quis tellus luctus aliquam. 
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>

            <div class="item">
                <p>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam bibendum eros vitae diam suscipit, eu egestas metus ultrices. In elit sapien, accumsan sed accumsan vel, mollis ac erat. Nulla at diam a nunc bibendum imperdiet. Suspendisse nec turpis hendrerit mi lacinia consectetur vitae eu sem. In sed velit quam. Donec interdum est in molestie iaculis. Vestibulum urna leo, scelerisque ut velit id, pellentesque fermentum lacus. Ut eu enim quis tellus luctus aliquam. 
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>

            <div class="item">
                <p>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam bibendum eros vitae diam suscipit, eu egestas metus ultrices. In elit sapien, accumsan sed accumsan vel, mollis ac erat. Nulla at diam a nunc bibendum imperdiet. Suspendisse nec turpis hendrerit mi lacinia consectetur vitae eu sem. In sed velit quam. Donec interdum est in molestie iaculis.
                </p>
                <div class="author">- Jonas Jonaitis</div>
            </div>
            
        </div>
    </div>
        <div class="index-back-button">
            <div class="wrap">
                <div class="go-back">
                    <a class="back-button-events" href="#"></a>
                </div>
            </div>
        </div>
    </div>

</div>



<footer class="index-footer">
    <div class="wrap">
        <div class="copyright">
            <span>Projektą remia:</span>
            <div class="footer-img1">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="94" height="45" viewBox="0 0 94 45">
                    <image xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF4AAAAtCAMAAAAQuiwzAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAC0FBMVEX///9/f39sbGxtbW2SkpJgYGDw8PDc3NxISEhjY2PHx8e4uLjJycn6+vri4uLW1ta7u7v39/fk5OTZ2dnn5+fh4eHo6Ojl5eW3t7fd3d35+fmysrLp6eny8vL+/v7FxcWrq6vu7u7r6+uzs7O0tLTm5ubx8fH7+/vj4+Pq6urGxsbCwsLY2NjNzc2vr6/AwMBGRkZra2slJSUwMDD9/f08PDyNjY2VlZUyMjIgICDV1dUjIyNnZ2f4+PhbW1ukpKRdXV3t7e0YGBiIiIgiIiIXFxckJCRvb28eHh5LS0vv7+8bGxt7e3stLS3X19fPz8+FhYXExMQzMzOfn5/29vZFRUVPT09wcHBUVFQWFhba2tomJiaDg4Nzc3O9vb1ERETR0dExMTFNTU0hISHIyMgvLy+Kioo/Pz+5ubmGhoYfHx/Dw8McHBwqKio0NDRMTEyurq6UlJSMjIxVVVXKyspTU1NHR0dhYWHg4OAdHR3Ly8unp6dWVlaJiYlfX193d3eCgoK+vr78/PycnJwUFBRRUVFaWlo7Ozs+Pj54eHgpKSleXl46OjouLi6bm5s2Nja6uroTExN9fX21tbUoKCgSEhKampo5OTmOjo41NTWhoaGioqKgoKBYWFjf398rKyt1dXUnJyesrKyYmJi/v7+qqqrs7OzQ0NBmZmaxsbFKSkpCQkLT09NoaGhlZWWPj489PT3BwcFQUFDS0tJXV1elpaX09PS2trY4ODh5eXmdnZ2AgIDb29tpaWnz8/Ojo6OLi4tycnKQkJCHh4eenp68vLz19fVOTk6oqKiZmZnOzs5BQUFiYmLe3t6BgYFJSUlZWVlDQ0Otra1AQEAODg4REREQEBCmpqaWlpYVFRVSUlKRkZEaGhp+fn58fHxcXFx0dHQsLCyEhITMzMw3Nzepqalqamp2dnZ6enpubm6wsLBkZGSTk5NxcXEZGRkAAADUxqh9AAAAAWJLR0TvuLDioQAAAAlwSFlzAAAPYQAAD2EBqD+naQAAB9tJREFUWMPt1olTE1keB/CfyB3RIKJyC6jcok0MxAaFEBsIwnIuSmAAwxkIXgMBOYVVkUPcAQW5jwEFj2FAQBCiBgSHQdRld9tFdBDBa4+/YV93EsAaa9edLWtrq+ZbBd106M/r/Pq9XzfAr/mfZZXKl8tqVVBT/3LR0AQt0Gas0WEw1q5jMHUZjPXrGQy9DQwGQ38jQ55NAJsNDI2MwcRIH8DUbIu5sZEFGFpuNQRDo22weTtYGVkDGNkAMPTBVhvs7MHMAXR3OJpu3In4XZgTazfGdsZc9DmcPasw3M4Vw3a77cXk2Qfg5O7BZcJmzBPAnbfflklogxfH28eU73sA/PzhN7wAAE4gQFAwuBMhoZ7w2zA4iB/SoflwAb41Aov8Co+KxmPQQIfthZggNoC3zMfxohEfn6AGibwkQzfE80WJdqJkforYBPHs1CPLvOAozbtxj5mz5Pxx4sTXeJp5SrojJsnIFJ48gAmzjDjYMu+XnYP43LwgyPfVAUD8KREUcAv5v0tzRvzpM766S/xprXT66s8WeQDNnzNMzbSJOwvFaUwsKZjwcYvDSkJKsRV82fk0xJdf+D3/GxxovqISIMWZf/GSZpW/lbBa5LDEe14+TfPR4gQ3mtesqSXqAnhu6fWWRKIWkVVNXG44ga3k49b4NDI3+TY1t7RyvqV5e66NcUob/2J7XJn/FeFV79plvqmc5qGD+JbmOwUbrp2x8b6udqMDa/dutBAI1sUv6xyaZ2PM2svs0KaNzYKb16lbW6SZjRvzL5p8x/OvEVd3nQNhdr6c/x6n+e5cTg/NM7FzPYL2W73r/NXVTfvUbbL67RU0fnugzd8KwEvP4Eb0YDVk3QH7PPeh6GF7cHC9agvS3BD+Vet9EXD3HrgOR8D9UyBrKVh9FEYKITJ3VF57gx2c8CbZ+Tt1noMR+1axhlB1LapjkH7WqOC/6wg0f6tHFtpQujbtgaz65A3b0Z3S0XaAseDxYDf0H8muD8Gs4xemthPx2eKM+7WFZjczfmCDSojYfhgjjimGn/gRR7e2RfbzTH6XK/u3STqMeJv76eYDUm0Nqw2PxiqmLH9A6wl/7IBw/gGBfOZ8IrFPZJ9ZnNU2W56q99WluRSn5/DNJuR3VVAB6k+VE/NTqbD5+bG+kOV9lrqC7yy70jBRTtDWrnYX5YT8w3S5SMFPch18Luqic6R/NJgKos8e5p4Zv+k6DlOhOft5x9lF0PYnj2CRt8zkzxFwKLySXFfG1SqQ85j42dehNF+659ryjCdZQ6flfLeLJVGZQV3eX2IFzpQeIA42m1EJOwFJPmzNtsznUdAye1L44qUokNsFHA0XsmS8hpel4DHsJ905HMNerV0tl4Xzh4lBpIyF0nxXfYRTkRnFH3LH+RT/qglAyY+/9pVRfOvsdivv49xUOx7ii3fAzNwST5QGdizg/c6KNukFIa0sivHQpHtOvE/a00WKn/W7TR0f43ou85VYvCrFs7kAt19yJycbNVx6M2NB7c0SjwZwtmm5oGiTZduX7tBDYh+4HVojztd8RhfnDk8bbU3FKhQfeRCSjrCLhCTNX5pVd8vM4baIUtHVN7ZA99wKHsPvsSYxjNdcTAjW0LK2k1dIHVWcjP19grszeV4NiF8XM5CfBZCDB1TMqMQvJI67s8eHb6mWt+bPOuBvI0VmXEYkVfvc9FbO4kq+Uh8eYrzXALveyS8cHZ/mUHygGHbqSfYKJqBP2PB89zT1+Utv9/ow82vcRkdZWjQvr7GxnufRizVfMhEyG3gaC4tDVSnvTVfwlckAi9hbdObVbbQuTcE+nvemH21Z9KbAFFaEtfKvAuW8p5Xn6EhvM7VWHiF6CsBAqOQrzv/CyI4o+ShDBNcpF/pAEcAVXMm3qX6ceRXVzwvdcyi+ahOlJisa8GsMLU09apKe/VRT6L//nzRkxFOPuBXRwlJ0gT+L1pnU4jTiWzrQ6MakVIc0OMWADJLs8RhEPc+KfAAfLIHcsJE8SZJkNLm+gyT7rnwYIguuM4B5t0/B8559PKr0PScWIAET9gJsf4jecxaIEWgltki55EwnkJUJqi6lJwC2eU/3zabCbKAt8U1REOeUcGuXX+m2+YU6ri77cbCgs1/BD1Mm3QHHFANk5QFYprxAey3Us7aiUNTw5PKcgu+CHpyJFm8057ZjqYhF8Y7wpsQA8egRruDdJaCYmF2UfL4O/Wp3VX4BqrN4oB+TH2n+EecDIalS8K8m+526jQESudk5N/e3zwZqE44mgkCKL/2rkh/EauW170avjtAj+ICKmT6Ddifu0gNY0GWie05chTVnJKUwpUHOx9yLDo/ZXQAt+Lvu55V7aP5v00DxmveUPIQJ5cW5ih7ea12IF/39f8cOIvIob9IaWFP16Os7dMnfcyqCOS+FocRRxPsAWQRuOrWZG8C56lIl0yXRd7Md8VVqNc17ofW8UPMPacBjE53KVfLiiCJNnih6vHjv3r2ILEt8RxBRg/mKh6FTOacpb9whKADxApcbgqAXcTGNOpCfbjnr0VUXVXpN2EbEBY0ivthPxak7mdMpfvMuAa+l+Y7Bf5lw1IXCamGkBpKOuX8/IpE4SyS9u7R6AEbnw0vgbf+ElsRxUSKRXMh9PScpCZjUg+Az760f/DRvhXg14y+X65owefjLZXzgM5f3r/n/yz8BVVKxvlSCI08AAAAASUVORK5CYII=" width="94" height="45"/>
                </svg>
            </div>
            <div class="footer-img2">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="87" height="34" viewBox="0 0 87 34">
                    <image xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAAAiCAMAAAAd+bUsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACTFBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoMCkSAAAAw3RSTlMA8MDkQMSwkBBU9NDPw6UOkXkUnIIlqJOfQ6JMnjpFm0hglQYVnR+PiSmBfHQwcKAxPlKAXgikAaMMqhILphaME3aLImFbSTVxfWUDiJYKdRcFabNjHYpOOW0CbpohNC17U8gnGxEEKDi+siQNho6EcnpEdxlojTdGIxggKhwJLwe8oTa6rlCHb1dafng7XDNnmU2xUedixywmf6f7vUpsc/XbT2Yaq6y3lIVBPD3M5ljT8x4/xl+Ykpe4uanJtK2Dtg/MryWkAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAAPYQAAD2EBqD+naQAAA/FJREFUSMe11otb01YYB+CfJTQFlLLVFuQyCaVCW1LQttSOUbFeANtROsBJmVJXKGC5TGBDiaIbWCaWOXAypxvu5i5Md3NednXbX7av7TbT2vBowe9pkpOT5O3Jl3Nygg0yWQakgsmQZcqRVshiPymXhSJ9N+tpuNk5ORtlUrGRZTflxs9T5j0DPKvarNbkswVbNKyGLSwqLsFzW0u5Mi3KdUXbKirz9Cg3GKtETZYInn1Y5nhamXhUK1CzfQfMsKCaalhYawHbTvvzdXih3rGrAbvTdqNhpiXuwrkHzN59+2mnsan5gAtpum4zk+iq3XixxaOnnVbW+1Jb+7q0N58WTcdBZL1MhUOddiDXl+BWyOXyrljpFSq1p3KZw3G35Ejc1XXjkJ+2R18NQFvbg976BjeCuYntPcJkM32xUv/AMSaU7A46h8zDNcOdgZHXTMdDGGUbqW6k0uOlY94hWvWO+Y3jeP2N/InkPGz4d+s+pk2RhyePJPchu66uiH0s94TJdPIxXDErdl0KJSYFpSD4IAgKwQE7T3GqRTiNKZ4PCkJ57CmcUdJ6/MzZZNf4pogVu731B+B/S9CZXGjSqdumwRRzM9y51rYw7LNDUyrjXl0P3q48P+vB3IWsgiQ3MtCC1C5MwoTTFu3A4E3R0aBw0PowWOrO4TA4BvPvXPTTeZ7pYhe2JLqRge6dNin33TwXRC5iY+1/t2jPQt9YtJ/2NS9eei8odi/vHmgJZpqk3KVCupfOGqrtErnD5O5YBOd8/woiAar44Crs3DWx++FHdGHH5YtSeQjptCjdB8wtR8XBuHt9HjAEaDQKKlv/x1TRNT8KFIrdT2K5Xfx0MJVrdYbx2bXxoObzJY3rRls4/AX0XzroeX7V7vkaXrcFpStT/gulRgMWvrGUPdp/wdancu1MP0LMafgmJnyooy7WjSvMJB3QzlPKrcxNoMaO8Vs36K9GS2wpXO23S6ny8OSR/H44+Z3+qbj4/gfr+rkB/kf+drzi1IyO70lyp1kK6gyWVmD7cVPX5P6VXizPDo4Nba6Kjt0mn4Srovk3Eq/QUPF6kpulFjhchVVO0zJNFA25qK5D1U9gOOjzgNCdZck8PBoJeSAXuHvrni82AW2FcH8bTULk2qlXuW//vCZXhaPUsF9WFs7Tm+DXBnLlhjslOKtCc8ca3O5Mtoy6N+uopfkneB+x9moLMXeJrTaswa2gEVbZQwO48SDi45hcmwoFNBB+C4ovzZBx0t9Rm36Xyf7478xIsZKfoTfWSOXog5u4y8HyQA3sumf+c/rEX3/De24pqbE5ku2dLF3tblaNVd1oHtJ06bs6W9rNoGSkxf4Dr7/UNksCB9MAAAAASUVORK5CYII=" width="87" height="34"/>
                </svg>
            </div>
            <span class="copy">Copyright &copy; 2018 Savininko pavadinimas. All rights reserved</span>
        </div>
    </div>
</footer>

<?php include 'includes/footer.php'; ?>